#include "client.h"
#include <omp.h>


/**
 * @private
 **/
Status
client_create(Client* client, Config* config, int n_sockets)
{
  Status st = Status::Ok;

	client->n_sockets = n_sockets;

  client->addr.sin_addr.s_addr = inet_addr(config->host);
  client->addr.sin_family = AF_INET;
  client->addr.sin_port = htons(config->port);

  client->timeout.tv_sec = 3;
  client->timeout.tv_usec = 0;

	// Reserva el espacio para los sockets
	client->sockets = (int *) malloc(n_sockets * sizeof(int));

	#ifdef COMPROBAR_RESPUESTAS
	puts("[Warning] El cliente esta configurado para comprobar las respuestas recibidas del server");
	#else
	puts("[Warning] El cliente no comprobora las respuestas del server");
	#endif

  return st; // st => Status
}

/**
 * @private
 **/
Status
client_reconfig_echo(Client* client, Config* config, int indice)
{
  Status st = Status::Ok;

  client->timeout.tv_sec = 0;
  client->timeout.tv_usec = 700000;

  if (setsockopt(
        client->sockets[indice], SOL_SOCKET, SO_RCVTIMEO, (char*)&client->timeout, sizeof(client->timeout)) <
  	   0) {
   	perror("[error] setsockopt");
   	return Status::Err;
  }

  return st;
}

/**
 * @private
 **/
Status
gestiona_respuesta(char *server_reply, int expected_size, int indice,
										Config *config, Client *client,Operation op, long aux)
{
	// Puntero al buffer de server_reply
	char* ptr = server_reply;
	char* limit = ptr + 2000;

  int size = 0;
  int chunks = 0;
  int read_size = 0;
  // loop de recepcion
  do {
  	ptr = (ptr + read_size);
		if ( ptr > limit ) {
			//printf("[Warning] PTR goes off the buffer (%p)\n", ptr);
			ptr = server_reply;
		}

    read_size = recv(client->sockets[indice], ptr, 2000, 0);

		if(read_size < 0){
			// DEPURACION DEL RECV
			switch(errno) {
				case EBADF: puts("[error] not a valid socket descriptor"); break;
				case ECONNRESET: puts("[error] Connection forcibly closed by a peer"); break;
				case EFAULT: puts("[error] Buffer + len access storage outside the callers address"); break;
				case EINTR: puts("[error] The recv() was interrupted by a signal"); break;
				case EINVAL: puts("[error] The request is not supported"); break;
				case EIO: puts("[error] Network transport failure"); break;
				case ENOBUFS: puts("[error] Insufficient sustem resources aviables"); break;
				case ENOTCONN: puts("[error] A recieve is attempted on a connection-oriented socket that is not connected"); break;
				case ENOTSOCK: puts("[error] The decriptor is for file, not for a socket"); break;
				case EOPNOTSUPP: puts("[error] The flags are not supported for this socket/protocol"); break;
				case ETIMEDOUT: puts("[error] timed out during transmission"); break;
				case EWOULDBLOCK: puts("[error] socket is in nonblocking and data is not aviable"); break;
				default:
				{
					if(read_size < 0){
						puts("[error] Error distinto");
						return Status::Err;
					}else {
						puts("Todo bien");
					}
				}
			}
		}else {
			chunks++;
			size += read_size;
		}
  } while (read_size > 0 && size < expected_size);
  if (config->debug) {
		switch(op) {
			case Operation::Echo:
				{
					// AUX = INIT_VALUE
					long value = echo_parse_response(server_reply);
					if(value == aux+1) {
						printf("====recv==== [%dB %d chunk(s)] echo: %ld\n", size, chunks, value);
					}else {
						printf("[echo] wrong answer: %ld\n", value);
					}
					printf("=============\n");
					break;
				}
			case Operation::MatMul:
				{
					// AUX = DIM
					printf("====recv==== [%dB %d chunk(s)] MatMul: C=A*B\n", size, chunks);
					float *C = matmul_parse_response(server_reply);
					*server_reply = '\0';
					if(C == nullptr) {
						puts("[error] MatMul response");
					}else {
						#ifdef COMPROBAR_RESPUESTAS
					/**	int dim = (int) aux;	// La dimension viene por aux
						float **Mtemp = matmul_alloc(dim);	// Reserva el espacio
						// Opera la matriz
						matmul_fill(Mtemp, dim);
						matmul_compute(Mtemp, dim);
						float *sol = Mtemp[2];
						// Compara resultados
						bool son_iguales = true;
						for(int i = 0; i < dim; i++) {
							for(int j = 0; j < dim; j++){
								if(sol[i * dim + j] != C[i * dim + j]) {
									son_iguales = false;
								}
							}
						}
						if(!son_iguales) {
							puts("[error] wrong answer");
							puts("EXPECTED MATRIX:");
							matmul_matrix_print(sol, dim);
							puts("RECIEVED MATRIX");
							matmul_matrix_print(C, dim);
						}
						// Libera la memoria
						matmul_destroy(Mtemp);**/
						#endif
					//	matmul_matrix_print(C, (int) aux);
					}
					printf("=============\n");
					break;
				}

			case Operation::Stream:
				{
					char *respuesta = stream_parse_response(server_reply);
					printf("====recv==== [%dB %d chunk(s)] stream: B = fx(A):\n %s \n", size, chunks,respuesta);
					//puts(respuesta);
					printf("=============\n");
					break;
				}
			default: perror("[error] Operation Recibida invalida"); return Status::Err;
		}
	}

	return Status::Ok;
}

/**
 * @private
 **/
Status
operacion_echo(Client *client, Config *config, char *buffer, char *server_reply, int indice){
	// Reconfigura para echo
  Status st = client_reconfig_echo(client, config, indice);
	// Comprueba la operacion
  if (st != Status::Ok) {
  	return st;
  }
  // Carga valor inicial
  long init_value = 111;
  echo_init(buffer, init_value);
  // Envia el echo y lo comprueba
  if (send(client->sockets[indice], buffer, strlen(buffer) + 1, 0) < 0) {
    puts("[error] send");
    return Status::Err;
  }
  if (config->debug) {
    printf("[done] send: '%s'\n", buffer);
  }

  int expected_size = 5 + 40 * sizeof(float); // 5 header
	// Limpia el buffer
	*buffer = '\0';
	return gestiona_respuesta(server_reply, expected_size, indice,
															config, client, Operation::Echo, init_value);
}

/**
 * @private
 **/
Status
operacion_matmul(Client *client, Config *config, char *buffer, char *server_reply, int indice) {
	// Dimension de la matriz
  unsigned int dim = 1000 %MAX_DIM;
  // Inicializa matrices en el buffer
  matmul_init(buffer, dim);
	printf("%s\n", buffer);

  if (send(client->sockets[indice], buffer, strlen(buffer) + 1, 0) < 0) {  // Comprueba el envio
  	puts("[error] send");
    return Status::Err;
  }
  if (config->debug) {
  	printf("[done] send: '%s'\n", buffer);
  }

	int expected_size = 8 + dim * dim * sizeof(float); // 8 header
	// Limpia el buffer
	*buffer = '\0';
	char respuesta[expected_size * 2];
	return gestiona_respuesta(respuesta, expected_size, indice,
															config, client, Operation::MatMul, dim);
}



// Declara los mensajes del stream
const char* message1 =
    "El ecologismo (en ocasiones llamado el movimiento verde o ambientalista) ";
const char* message2 = "es un variado movimiento político, social y global, que defiende la "
                         "protección del medio ambiente. ";
const char* message3 =
    "Recomiendo la película 'Barbacana, la huella del lobo', recientemente proyectada en cines "
    "y rodada durante 12 años. Refleja el conflicto por la conservación del lobo ibérico y "
    "pone de manifiesto la ineficacia de las medidas actuales.";



/**
 * @private
 **/
Status
operacion_stream(Client *client, Config *config, char *buffer, char *server_reply, int indice) {
	// Inicializa el stream en el buffer
    stream_init(buffer);

    if (send(client->sockets[indice], buffer, strlen(buffer) + 1, 0) < 0) {  // Comprueba el envio
	    puts("[error] send");
        return Status::Err;
    }
    if (config->debug) {
        printf("[done] send: '%s'\n", buffer);
    }

    if (send(client->sockets[indice], message1, strlen(message1), 0) < 0) {   // Envia un mensaje
    	puts("[error] send");
        return Status::Err;
    }
    if (config->debug) {
    	printf("[done] send: '%s'\n", message1);
    }
    if (send(client->sockets[indice], message2, strlen(message2), 0) < 0) {  // Envia otro mensaje
    	puts("[error] send");
        return Status::Err;
    }
    if (config->debug) {
        printf("[done] send: '%s'\n", message2);
    }
    if (send(client->sockets[indice], message3, strlen(message3) + 1, 0) < 0) { // Envia ultimo mensaje + \0
        puts("[error] send");
        return Status::Err;
    }
    if (config->debug) {
        printf("[done] send: '%s'\n", message3);
    }

	// Limpia el buffer
	*buffer = '\0';

	int expected_size = 8 + 40 * sizeof(char); // 8 header
	return gestiona_respuesta(server_reply, expected_size, indice,
															config, client, Operation::Stream, 0);
}

/**
 * @private
 **/
Status
crear_sockets(Client *client, Config *config) {
	// Loop para crear los n sockets
	int socket_fd = -1;
	for(int i = 0; i < client->n_sockets; i++) {

		// Genera el socket
		socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  	if (socket_fd == -1) {
    	perror("[error] socket");
    	return Status::Err;
  	}
  	if (config->debug) {
    	puts("[done] socket");
  	}

		// Configura el socket
		if (setsockopt(
        socket_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&client->timeout, sizeof(client->timeout)) < 0) {
    	perror("[error] setsockopt");
    	return Status::Err;
  	}

  	if (setsockopt(
        socket_fd, SOL_SOCKET, SO_SNDTIMEO, (char*)&client->timeout, sizeof(client->timeout)) < 0) {
  	  perror("[error] setsockopt");
  	  return Status::Err;
  	}

		// Asigna el socket
		client->sockets[i] = socket_fd;
	}
	// Retorna OK
	return Status::Ok;
}

/**
 * @private
 **/
Status
client_connect(Client* client, Config* config)
{
	// Loop para conectar todos los sockets
	for(int i = 0; i < client->n_sockets; i++) {	// Para cada socket
		// Intenta conectar con el server

  		if (connect(client->sockets[i], (const sockaddr*)&client->addr, sizeof(client->addr)) < 0) {
    		//perror("[error] connect");
        printf("El server no ha establecido la conexion con el socket %d, asi que cierro el cliente, por error de conexion\n",i );
    		return Status::Err;
  		}
	}
  if (config->debug) {
    puts("[done] connect");
  }
  return Status::Ok;
}

/**
 * @private
 **/
Status
client_disconnect(Client* client, Config* config)
{
	// Loop para desconectar los sockets
	for(int i = 0; i < client->n_sockets; i++) {
  	close(client->sockets[i]);
	}
  if (config->debug) {
   	puts("[done] disconnect");
 	}

	// Libera la memoria
	free( client->sockets );
  return Status::Ok;
}

Status
client(Config* config)
{
	int n_hilos;
	// Comprueba conexiones a realizar
	if ( config->connections == 0 ){
		n_hilos = omp_get_num_procs() + 20;	// Genera overhead a posta
	} else {
		n_hilos = config->connections;
	}

  Status st = Status::Ok;
  Client client; // Objeto cliente

  // BUFFERS
  //char *buffer = (char *)malloc(1000* sizeof(char));
  //char *server_reply =  (char *)malloc(2000 * sizeof(char)); //message[1000] la quito que no se usa
  char buffer[1000];
  char server_reply[2000];
  // Crea el cliente (st=>Status)
  st = client_create(&client, config, n_hilos);
  if (st != Status::Ok) {  // Comprueba el cliente
    return st;
  }

	// Crea los sockets para multiples solicitudes al servidor
	st = crear_sockets(&client, config);

	// A partir de aqui hay n_hilos sockets abiertos


	// Comprueba el resultado de crear los socket
	if(st != Status::Ok) {
		perror("[error] crea_sockets");
		return st;
	}
	if(config->debug) {
		puts("[done] Sockets creado");
	}

	// Conecta el cliente
  st = client_connect(&client, config);
  if (st != Status::Ok) {  // Comprueba la conexion
    return st;
  }



	/* REGION PARALELA */
	#pragma omp parallel private(buffer,server_reply, st) shared(client, config) num_threads(n_hilos)
	{
		#pragma omp single
		{
		// Variables para el loop principal
		bool continua = true;
  	unsigned int times = 0;

  	unsigned int connections = config->connections;
		Protocol protocol = config->protocol;
  	Operation operation = operation_from_protocol(protocol);

		// Obtiene el hilo que esta ejecutando
		//int indice = omp_get_thread_num();

  	while ( continua ) {
		#pragma omp task
		{
			// COMPROBAR OPERACION
			switch(operation) {
				case Operation::Echo: st = operacion_echo(&client, config, buffer, server_reply, omp_get_thread_num()); break;
				case Operation::MatMul: st = operacion_matmul(&client, config, buffer, server_reply, omp_get_thread_num()); break;
				case Operation::Stream: st = operacion_stream(&client, config, buffer, server_reply, omp_get_thread_num()); break;
				default: puts("[cmd] invalid operation"); break;
			}

			// Comprueba st
			if (st != Status::Ok) {
				printf("[Error] Operation return bad status on thread %d iteration %d", omp_get_thread_num(), times);
			}
		} // Fin de la task

    	/* if connections == 0, loop forever */
    	if (connections) {
      	times++;
      	if (times >= connections) {
       		continua = false;
	     	 	}
    	}
  	} // Fin loop
		#pragma omp taskwait
	}	/* FIN DEL SINGLE */
	} /* FIN REGION PARALELA */

  // Desconecta el cliente
  st = client_disconnect(&client, config);
  if (st != Status::Ok) {
    return st;
  }
  if (config->debug) {
  	puts("[exit]");
  }

  return Status::Ok;
}
