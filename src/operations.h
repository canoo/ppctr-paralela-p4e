#ifndef FLAME_OPERATIONS_H
#define FLAME_OPERATIONS_H

#include <cstring>

enum class Operation
{
  Invalid = 0,
  Echo = 1,
  MatMul = 2,
  Stream = 3,
};

Operation
match_operation(char* str);

#endif // FLAME_OPERATIONS_H
