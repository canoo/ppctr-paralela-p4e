#ifndef FLAME_CLIENT_H
#define FLAME_CLIENT_H

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include "omp.h"
#include <errno.h>

#include "config.h"
#include "operations/echo.h"
#include "operations/matmul.h"
#include "operations/stream.h"

struct Client
{
	int n_sockets;
  int *sockets;
  struct sockaddr_in addr;
  struct timeval timeout;
};

Status
client(Config* config);

#endif // FLAME_CLIENT_H
