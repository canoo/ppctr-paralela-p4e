cmake_minimum_required(VERSION 3.0)
project(flame)

set (CMAKE_CXX_STANDARD 14)

set (CMAKE_CXX_FLAGS "-g -Wall -O2 -DNDEBUG -DBOOST_UBLAS_NDEBUG")
find_package(OpenMP)
if(OPENMP_FOUND)
        set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
        set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
        set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

set (SOURCES
    src/config.cpp
    src/operations.cpp
    src/operations/echo.cpp
    src/operations/matmul.cpp
    src/operations/stream.cpp
    src/client.cpp
    src/server.cpp
)
add_executable(flame src/flame.cpp ${SOURCES})

#target_link_libraries (flame LINK_PUBLIC m)
