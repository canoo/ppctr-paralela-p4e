#include "echo.h"

void
echo_init(char* buffer, long value)
{
  sprintf(buffer, "op:%s:%ld%c", "echo", value,'\0');
}

long
echo_parse(char* buffer)
{
  long val;
  sscanf(buffer, "op:echo:%ld", &val);
  return val;
}

void
echo_init_response(char* buffer, long value)
{
  sprintf(buffer, "%s:%ld%c", "echo", value + 1,'\0');
}

long
echo_parse_response(char* buffer)
{
  if (strncmp(buffer, "echo:", 5) != 0) {
    return 0;
  }
  long val;
  sscanf(buffer, "echo:%ld", &val);
  return val;
}
