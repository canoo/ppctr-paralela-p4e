.PHONY: build build/debug build/release

default: build

build:
	mkdir -p build/release; cd build/release; \
	cmake ../.. -DCMAKE_BUILD_TYPE=Release; make
clean:
	rm -rf build

build/debug:
	mkdir -p build/debug; cd build/debug; \
	cmake ../.. -DCMAKE_BUILD_TYPE=Debug; make

build/release:
	mkdir -p build/release; cd build/release; \
	cmake ../.. -DCMAKE_BUILD_TYPE=Release; make

clang-format:
	find src -regex '.*\.\(h\|cpp\)' -type f -exec clang-format -i {} \;
