#include "operations.h"

Operation
match_operation(char* str)
{
  Operation op;
  if (strncmp("op:echo:", str, 8) == 0) {
    op= Operation::Echo;
  } else if (strncmp("op:matmul:", str, 10) == 0) {
      op= Operation::MatMul;
  } else if (strncmp("op:stream:", str, 10) == 0) {
    op =  Operation::Stream;
  } else {
    op = Operation::Invalid;
  }
  return op;
}
