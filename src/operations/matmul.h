#ifndef FLAME_OPERATIONS_MATMUL_H
#define FLAME_OPERATIONS_MATMUL_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>
#include <stdint.h>

#define RAND (rand() % 3)
#define MAX_DIM 65535

void
matmul_init(char* buffer, unsigned int dim);
int
matmul_parse(char* buffer);
unsigned int
matmul_init_response(char* buffer);

float*
matmul_parse_response(char* buffer);

float **matmul_compute(int n_a_rows, int n_a_cols, float *const *a, int n_b_cols, float *const *b);
float **mat_gen_random(int n_rows, int n_cols,int gen);
float **mat_init(int n_rows, int n_cols);
float **mat_transpose(int n_rows, int n_cols, float *const* a);

float sdot_1(int n, const float *x, const float *y);
float sdot_8(int n, const float *x, const float *y);
float sdot_sse(int n, const float *x, const float *y);
void mat_destroy(float **m);

float**
matmul_alloc(int dim);
float*
matmul_matrix_alloc(int dim);
void
matmul_destroy(float** Ms);

void
matmul_fill(float** Ms, int dim);
void
matmul_matrix_fill(float* M, int dim);
void
matmul_matrix_print(float* M, int dim);



#endif // FLAME_OPERATIONS_MATMUL_H
