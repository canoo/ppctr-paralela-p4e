# FLAME

## Brainstorming, initial ideas...
Paralelizacion del programa (tipo tasks) en el server, ataque paralelo con el client, vectorizacion de matmul(no implementado en este deadline), tambien estuvo la idea de precomputar matmul y refactorizar codigo. Las ideas del client son de ivan y las del server de Alejandro Cano

## After every championship, new ideas...

El cliente va a atacar con tasks en vez de paralelizar todo para una mejor sincronización, ahora el cliente limpia los buffers de envío. El buffer de respuesta del servidor pasa a ser circular.(I)

La gestión de la respuesta del servidor en el cliente se ha refactorizado para tenerla en una sóla función. (I)
Se ha implementado la comprobación de la multiplicación de matrices. (I)
(Aunque no se usa porque hemos llegado a la conclusion de que no merece la pena)

Con connections == 0 (ataque infinito) el cliente produce a proposito overhead, creando más hilos que procesadores tenga la máquina, y los aprovecha para mandar solicitudes.(I)

Hemos optimizado mucho las multiplicaciones, con la vectorizacion y la gestion eficiente de la memoria, para poder responder a matrices grandes (1000 x 1000) de manera no muy costosa. Ademas hemos añadido unos flags en la compilacion, para que el compilador optimice nuestro codigo/reordene pero sin cambiar el resultado.(A)

Hemos hecho pequeñas mejoras en general, refactorizando cosillas para ahorra gestion en memoria en general, como creacion de atributos para compartir constantes (sobretodo en el stream).(A)

El server esta distribuido de tal manera que un hilo unico accepta peticiones de cliente, y crea una task para que otro hilo haga la "comunicacion" a traves de ese socket. Es decir el hilo principal solamente reparte trabajo a los demas. La unica consecuencia "negativa" de esto es que a la hora de atender peticiones, se va a contestar antes la peticion menos costosa, no la que ha llegado antes.(A)

El cliente indica cuando el server no accepta sus petiones. (A)

He decido no hacer algunos prints de debug, sobre todo los que son muy largos (matrices de 1000x 1000), debido a que retrasa mucho las contestaciones.

Arreglos de algunos bugs entre los dos, en general como:
    -Core dumped en Stream
    -Core dumped en MatMuls(se iba del stack), ahora se sigue llendo pero con dimensiones muy grandes, lo cual creemos que es obvio.
    

Opinion general, Alejandro Cano:

Me quedo con buenas sensaciones despues de haber terminado, la verdad que siento que he aprendido mucho con esta practica, y no solo de paralelismo, sino
de muchas otras cosas, ademas de coger experiencia en algo que se sale un poco de lo habitual en la uni, que siempre esta bien.
Personalmente es la practica con la que mas he aprendido, junto con la 1, para mi han sido las mas gratificantes.

A pesar de decir que me haya gustado, me ha parecido una practica dura, dificil y tienes que tener mucha paciencia si quieres entender todo 
lo que hace el programa y lo que quieres hacer. He "perdido" (realmente he aprendido) mucho tiempo con problemas sobre sockets, conexiones, ejecuciones 
diferentes en diferentes distros de linux....Que la verdad que al principio te frustran, pero luego con el tiempo acabas entendiendo y te sienta bien 
haberlo hecho por tu cuenta. Para empezar, nos diste un codigo enorme, sin refactorizar, sin comentar y con cosas que no habiamos visto en la vida
las cuales me sonaban a chino, y que algun comentario se hubiese agradecido, aunque realmente entiendo que lo que quieres es 
que nos enfrentemos a esto nosotros solos y eso al final esta bien porque aunque requiere trabajo, siempre vale mas lo que te curras por ti mismo que 
lo que te dan ya hecho, asi que me quedo con que esto y que nos sirve para ganar experiencia para el futuro, lo cual es algo muy bueno. Si nos hubieses
dado las cosas algo mas refactorizadas y con mas comentarios tal vez la practica hubiese sido mas facil y no la hubiera abandonado tanta gente, pero de 
seguro que habriamos aprendido menos y la verdad que visto como lo hemos enviado/lo aprendido, no cambiaria nada.

He de añadir que no fuimos a la primera reunion, y eso ha podido suponer una dificultad extra, ya que no tengo conocimiento de lo que visteis (Tuve mucha
carga de trabajo y ni siquiera me habia planteado hacer la practica hasta despues).

Tiempo aproximado: no estoy seguro del dato que voy a dar, pero he estado mucho mas tiempo en comparacion con las demas practicas, aproximadamente yo creo
que 50 h - 60 h pero la verdad que no lo sé, he estado algun tiempo atascado con algun error: problemas con git, la distro, problemas con los sockets y he probado
ideas que me surgian....

Valoración de Iván Monroy: 

Yo, como mi compañero considero que la práctica me ha servido para aprender a leer código ajeno, para después mejorarlo, a buscar soluciones por mi cuenta a problemas
que han ido surgiendo y a buscar una optimización del código que antes pues no hacía. 
Como sugerencia, facilitar la lectura del código con comentarios, que creo que es donde más tiempo hemos invertido, aunque como dice mi compañero también nos ayuda a estudiar
el código por nuestra cuenta.

Tiempo dedicado: Aproximadamente 25 horas, sobretodo de lectura de código y solucionando problemas; más que programando propiamente.



